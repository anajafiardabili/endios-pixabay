package de.endios.pixabay.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import de.endios.pixabay.data.photo.DefaultPhotoRepository
import de.endios.pixabay.data.photo.PhotosRepository

@Module
@InstallIn(ViewModelComponent::class)
interface RepositoryModule {
    @Binds
    fun providePhotosRepository(photosRepository: DefaultPhotoRepository): PhotosRepository
}
