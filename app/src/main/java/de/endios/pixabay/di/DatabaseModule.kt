package de.endios.pixabay.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.endios.pixabay.db.AppDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext appContext: Context
    ) = Room.databaseBuilder(
        appContext,
        AppDatabase::class.java,
        "endios_submission_database"
    ).build()

    @Singleton
    @Provides
    fun providePhotosDao(db: AppDatabase) = db.photosDao()

    @Singleton
    @Provides
    fun provideRemoteKeysDao(db: AppDatabase) = db.remoteKeysDao()
}
