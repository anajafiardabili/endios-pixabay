package de.endios.pixabay.utils

import java.net.URLEncoder

// according to https://pixabay.com/api/docs/ the query should be URL encoded
fun prepareQueryString(query: String): String = URLEncoder.encode(query, "utf-8")
