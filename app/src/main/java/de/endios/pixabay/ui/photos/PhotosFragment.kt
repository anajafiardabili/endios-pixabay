package de.endios.pixabay.ui.photos

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import de.endios.pixabay.databinding.FragmentPhotosBinding
import de.endios.pixabay.ui.common.BindingFragment
import de.endios.pixabay.ui.common.hideSoftKeyboard
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi // for flatMapLatest
@AndroidEntryPoint
class PhotosFragment: BindingFragment<FragmentPhotosBinding>() {

    private val viewModel by viewModels<PhotosViewModel>()
    private lateinit var adapter: PhotosAdapter

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPhotosBinding
        get() = FragmentPhotosBinding::inflate

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        initSearch()
        initSwipeToRefresh()
        initClickListener()
    }

    private fun initClickListener() {
        binding?.apply {
            retryButton.setOnClickListener { adapter.retry() }
        }
    }

    private fun initAdapter() {
        binding?.apply {
            adapter = PhotosAdapter(
                onPhotoClicked = {
                    findNavController().navigate(
                        PhotosFragmentDirections.showConfirmationDialog(it)
                    )
                }
            )

            lstPhotos.adapter = adapter

            lifecycleScope.launchWhenCreated {
                adapter.loadStateFlow.collect { loadStates ->
                    swipeRefresh.isRefreshing = loadStates.mediator?.refresh is LoadState.Loading
                }
            }

            lifecycleScope.launchWhenCreated {
                viewModel.photos.collectLatest {
                    adapter.submitData(it)
                }
            }

            lifecycleScope.launch {
                adapter.loadStateFlow.collect { loadState ->
                    val isListEmpty = loadState.refresh is LoadState.NotLoading && adapter.itemCount == 0
                    // show empty list
                    emptyList.isVisible = isListEmpty
                    // Only show the list if refresh succeeds, either from the the local db or the remote.
                    lstPhotos.isVisible = loadState.source.refresh is LoadState.NotLoading || loadState.mediator?.refresh is LoadState.NotLoading
                    // Show loading spinner during initial load or refresh.
                    progressBar.isVisible = loadState.mediator?.refresh is LoadState.Loading
                    // Show the retry state if initial load or refresh fails.
                    retryButton.isVisible = loadState.mediator?.refresh is LoadState.Error && adapter.itemCount == 0
                    // show any error, regardless of whether it came from RemoteMediator or PagingSource
                    val errorState = loadState.source.append as? LoadState.Error
                        ?: loadState.source.prepend as? LoadState.Error
                        ?: loadState.append as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error
                    errorState?.let {
                        Snackbar.make(
                            root,
                            "\uD83D\uDE28 Wooops ${it.error}",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }
    }

    private fun initSwipeToRefresh() {
        binding?.swipeRefresh?.setOnRefreshListener { adapter.refresh() }
    }

    private fun initSearch() {
        binding?.apply {
            edtQuery.setText(PhotosViewModel.DEFAULT_QUERY)
            edtQuery.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    updatePhotosFromUserInput()
                    true
                } else {
                    false
                }
            }
            edtQuery.setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    updatePhotosFromUserInput()
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun updatePhotosFromUserInput() {
        binding?.edtQuery?.text?.trim().toString().let {
            if (it.isNotBlank()) {
                hideSoftKeyboard()
                viewModel.fetchPhotos(it)
            }
        }
    }
}
