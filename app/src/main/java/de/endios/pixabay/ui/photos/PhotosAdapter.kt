package de.endios.pixabay.ui.photos

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import de.endios.pixabay.databinding.ItemPhotoBinding
import de.endios.pixabay.model.Photo
import de.endios.pixabay.ui.common.OnPhotoClicked

/***********************************************************************************************
 * Comparator to optimize list item updates
 */

object PhotoComparator : DiffUtil.ItemCallback<Photo>() {
    override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
        return oldItem == newItem
    }
}

/***********************************************************************************************
 * View Holder
 */
class PhotoViewHolder(
    private val binding: ItemPhotoBinding,
    private val context: Context,
    private val onPhotoClicked: OnPhotoClicked
): RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Photo?) {
        item?.let {
            binding.photo = it
            itemView.setOnClickListener {
                onPhotoClicked(item)
            }
        }
    }

    companion object {
        fun from(parent: ViewGroup, onPhotoClicked: OnPhotoClicked): PhotoViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemPhotoBinding.inflate(layoutInflater, parent, false)
            return PhotoViewHolder(binding, parent.context, onPhotoClicked)
        }
    }
}

/***********************************************************************************************
 * Adapter
 */

class PhotosAdapter(
    private val onPhotoClicked: OnPhotoClicked
) : PagingDataAdapter<Photo, PhotoViewHolder>(PhotoComparator) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PhotoViewHolder {
        return PhotoViewHolder.from(parent, onPhotoClicked)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }
}
