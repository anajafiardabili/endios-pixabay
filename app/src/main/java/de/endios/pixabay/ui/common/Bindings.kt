package de.endios.pixabay.ui.common

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import de.endios.pixabay.R

@BindingAdapter("string")
fun setText(textView: TextView, str: String) {
    textView.text = str
}

@BindingAdapter("likes")
fun setLikes(textView: TextView, numberOfLikes: Int) {
    if (numberOfLikes == 1) {
        textView.text = textView.context.getString(R.string.like)
    } else {
        textView.text = textView.context.getString(R.string.likes, numberOfLikes)
    }
}

@BindingAdapter("comments")
fun setComments(textView: TextView, numberOfComments: Int) {
    if (numberOfComments == 1) {
        textView.text = textView.context.getString(R.string.comment)
    } else {
        textView.text = textView.context.getString(R.string.comments, numberOfComments)
    }
}

@BindingAdapter("tags")
fun setImageTags(tags: ChipGroup, tagsStr: String) {
    tags.removeAllViews()
    tagsStr.replace(" ", "").split(",").forEach { tagName ->
        val tag = Chip(tags.context)
        tag.text = tagName
        tags.addView(tag)
    }
}

@BindingAdapter("imageUrl")
fun loadImage(imgView: ImageView, url: String) {
    Glide.with(imgView.context)
        .load(url)
        .centerCrop()
        .placeholder(R.drawable.illustration_placeholder)
        .error(ColorDrawable(Color.RED))
        .fallback(ColorDrawable(Color.GRAY)) // when the requested url/model is null
        .into(imgView)
}
