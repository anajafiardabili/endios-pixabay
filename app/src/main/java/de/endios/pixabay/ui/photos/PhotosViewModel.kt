package de.endios.pixabay.ui.photos

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import de.endios.pixabay.data.photo.PhotosRepository
import de.endios.pixabay.utils.prepareQueryString
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest
import javax.inject.Inject

@HiltViewModel
class PhotosViewModel @Inject constructor(
    private val photosRepository: PhotosRepository,
    private val savedStateHandle: SavedStateHandle,
): ViewModel() {
    companion object {
        const val KEY_QUERY = "query"
        const val DEFAULT_QUERY = "fruits"
    }

    @ExperimentalCoroutinesApi // for flatMapLatest
    val photos = savedStateHandle.getLiveData<String>(KEY_QUERY)
        .asFlow()
        .flatMapLatest { query ->
            val encodedQuery = prepareQueryString(query)
            photosRepository.getPhotos(encodedQuery)
        }
        // cachedIn() shares the paging state across multiple consumers of posts,
        // e.g. different generations of UI across rotation config change
        .cachedIn(viewModelScope)

    init {
        if (!savedStateHandle.contains(KEY_QUERY)) {
            savedStateHandle.set(KEY_QUERY, DEFAULT_QUERY)
        }
    }

    fun fetchPhotos(query: String) {
        if (!shouldFetchPhotos(query)) return
        savedStateHandle.set(KEY_QUERY, query)
    }

    private fun shouldFetchPhotos(query: String): Boolean {
        return savedStateHandle.get<String>(KEY_QUERY) != query
    }
}
