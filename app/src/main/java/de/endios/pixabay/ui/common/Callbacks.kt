package de.endios.pixabay.ui.common

import de.endios.pixabay.model.Photo

typealias OnPhotoClicked = (Photo) -> Unit
