package de.endios.pixabay.ui.photo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import de.endios.pixabay.databinding.FragmentPhotoBinding
import de.endios.pixabay.ui.common.BindingFragment

class PhotoFragment: BindingFragment<FragmentPhotoBinding>() {
    private val args: PhotoFragmentArgs by navArgs()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPhotoBinding
        get() = FragmentPhotoBinding::inflate

    override fun setup() {
        binding?.photo = args.photo
    }
}
