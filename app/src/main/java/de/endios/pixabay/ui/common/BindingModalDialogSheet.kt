package de.endios.pixabay.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class BindingModalDialogSheet<ViewBindingType : ViewBinding> : BottomSheetDialogFragment() {
    private var _binding: ViewBindingType? = null
    protected val binding get() = _binding
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> ViewBindingType

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = bindingInflater.invoke(inflater, container, false)
        setup()
        return requireNotNull(_binding).root
    }

    open fun setup() {}

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
