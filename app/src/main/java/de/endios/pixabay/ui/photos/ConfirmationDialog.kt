package de.endios.pixabay.ui.photos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.endios.pixabay.databinding.DialogConfirmationBinding
import de.endios.pixabay.ui.common.BindingModalDialogSheet

class ConfirmationDialog: BindingModalDialogSheet<DialogConfirmationBinding>() {
    private val args: ConfirmationDialogArgs by navArgs()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogConfirmationBinding
        get() = DialogConfirmationBinding::inflate

    override fun setup() {
        binding?.apply {
            btnClose.setOnClickListener {
                findNavController().popBackStack()
            }

            btnContinue.setOnClickListener {
                findNavController().navigate(
                    ConfirmationDialogDirections.continueToPhotoDetailsScreen(
                        args.photo
                    )
                )
            }
        }
    }
}
