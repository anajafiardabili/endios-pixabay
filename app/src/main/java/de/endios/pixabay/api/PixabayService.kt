package de.endios.pixabay.api

import de.endios.pixabay.DEFAULT_PAGE_SIZE
import de.endios.pixabay.PIXABAY_KEY
import de.endios.pixabay.model.PixabayResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayService {
    @GET(".")
    suspend fun getPhotos(
        @Query("key") key: String = PIXABAY_KEY,
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int = DEFAULT_PAGE_SIZE
    ): PixabayResponse
}
