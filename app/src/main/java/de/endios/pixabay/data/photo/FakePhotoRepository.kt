package de.endios.pixabay.data.photo

import androidx.paging.PagingData
import de.endios.pixabay.model.Photo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakePhotoRepository  : PhotosRepository {
    override fun getPhotos(query: String): Flow<PagingData<Photo>> =
        flow {
            emit(
                PagingData.from(listOf(
                    Photo(1, 1, "", "", "", "", 0, 0, ""),
                    Photo(2, 2, "", "", "", "", 0, 0, ""),
                    Photo(3, 3, "", "", "", "", 0, 0, "")
                ))
            )
        }
}
