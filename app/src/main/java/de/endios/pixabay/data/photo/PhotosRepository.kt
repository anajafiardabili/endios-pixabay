package de.endios.pixabay.data.photo

import androidx.paging.PagingData
import de.endios.pixabay.model.Photo
import kotlinx.coroutines.flow.Flow

interface PhotosRepository {
    fun getPhotos(query: String): Flow<PagingData<Photo>>
}
