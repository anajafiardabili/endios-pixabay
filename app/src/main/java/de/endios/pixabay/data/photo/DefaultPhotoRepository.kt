package de.endios.pixabay.data.photo

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import de.endios.pixabay.DEFAULT_PAGE_SIZE
import de.endios.pixabay.api.PixabayService
import de.endios.pixabay.db.AppDatabase
import de.endios.pixabay.model.Photo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DefaultPhotoRepository @Inject constructor(
    private val pixabayService: PixabayService,
    private val localDatabase: AppDatabase
): PhotosRepository {
    override fun getPhotos(query: String): Flow<PagingData<Photo>> {
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = DEFAULT_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = PhotosRemoteMediator(
                query,
                pixabayService,
                localDatabase
            ),
            // TODO: we can improve this. we cannot get the same results from
            //  local cache based on query string.
            pagingSourceFactory = {
                localDatabase.photosDao().allPhotos(
                    query
                )
            }
        ).flow
    }
}