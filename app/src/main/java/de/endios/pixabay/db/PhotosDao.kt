package de.endios.pixabay.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.endios.pixabay.model.Photo

@Dao
interface PhotosDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(photos: List<Photo>)

    @Query("SELECT * FROM photos WHERE `query` LIKE :query")
    fun allPhotos(query: String): PagingSource<Int, Photo>

    @Query("DELETE FROM photos")
    suspend fun clearPhotos()
}
