package de.endios.pixabay.db

import androidx.room.Database
import androidx.room.RoomDatabase
import de.endios.pixabay.model.Photo
import de.endios.pixabay.model.RemoteKey

/**
 * Note that exportSchema should be true in production databases.
 */
@Database(
    entities = [
        Photo::class,
        RemoteKey::class
    ], version = 1, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun photosDao(): PhotosDao
    abstract fun remoteKeysDao(): RemoteKeysDao
}
