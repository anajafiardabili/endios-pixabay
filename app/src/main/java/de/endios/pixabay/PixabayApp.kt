package de.endios.pixabay

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

const val DEFAULT_PAGE_SIZE = 10
const val PIXABAY_KEY = "24566236-ac7240ad7f1262ff35ac94056"

@HiltAndroidApp
class PixabayApp : Application()
