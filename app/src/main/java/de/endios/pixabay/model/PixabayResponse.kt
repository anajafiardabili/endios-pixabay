package de.endios.pixabay.model

data class PixabayResponse(
    val total: Int,
    val totalHits: Int,
    val hits: List<Photo>
)
