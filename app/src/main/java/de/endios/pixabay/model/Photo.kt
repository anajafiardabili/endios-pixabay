package de.endios.pixabay.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "photos")
data class Photo(
    @PrimaryKey(autoGenerate = true) val localId: Long,
    val id: Long,
    val previewURL: String,
    val webformatURL: String,
    val tags: String,
    val user: String,
    val likes: Int,
    val comments: Int,
    var query: String
): Serializable
