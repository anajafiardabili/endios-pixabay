package de.endios.pixabay.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import de.endios.pixabay.R
import de.endios.pixabay.launchFragmentInHiltContainer
import de.endios.pixabay.mock.MockServer
import de.endios.pixabay.mock.successDispatcher
import de.endios.pixabay.ui.photos.PhotosFragment
import de.endios.pixabay.utils.testing.EspressoIdlingResource
import de.endios.pixabay.utils.waitForResults
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.Matchers.anyOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class PhotosScreenTest {
    private lateinit var idlingResourceEspresso: androidx.test.espresso.IdlingResource

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        idlingResourceEspresso = EspressoIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResourceEspresso)

        MockServer.server.dispatcher = successDispatcher()
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResourceEspresso)
    }

    @Test
    fun test_startingPhotosScreenShouldLoadQueryResultsForFruits() {
        waitForResults()

        launchFragmentInHiltContainer<PhotosFragment>(themeResId = R.style.Theme_Endiospixabay)
        waitForResults()

        onView(anyOf(withText("Kranich17"))).check(matches(isDisplayed()))
        waitForResults()
    }
}
