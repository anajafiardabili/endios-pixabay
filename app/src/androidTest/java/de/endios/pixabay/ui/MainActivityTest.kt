package de.endios.pixabay.ui

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import de.endios.pixabay.R
import de.endios.pixabay.utils.testing.EspressoIdlingResource
import de.endios.pixabay.utils.waitForResults
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {
    private lateinit var idlingResourceEspresso: androidx.test.espresso.IdlingResource

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        idlingResourceEspresso = EspressoIdlingResource.countingIdlingResource
        IdlingRegistry.getInstance().register(idlingResourceEspresso)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResourceEspresso)
    }

    @Test
    fun test_clickingOnPhotoShouldShowDialog() {
        waitForResults()

        launchActivity<MainActivity>()
        waitForResults()

        onView(anyOf(withText("Kranich17"))).perform(click())
        waitForResults()
        onView(withId(R.id.btn_continue)).check(matches(ViewMatchers.isDisplayed()))

        onView(withId(R.id.btn_continue)).perform(click())
        waitForResults()

        onView(Matchers.anyOf(withText("Kranich17"))).check(matches(ViewMatchers.isDisplayed()))
        onView(Matchers.anyOf(withText("215 likes"))).check(matches(ViewMatchers.isDisplayed()))
    }
}
