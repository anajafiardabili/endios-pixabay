package de.endios.pixabay.mock

import okhttp3.mockwebserver.MockWebServer

class MockServer {
    companion object {
        val server = MockWebServer()
    }
}
