package de.endios.pixabay.mock

import dagger.Module
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import de.endios.pixabay.di.NetworkModule

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [NetworkModule::class]
)
class MockNetworkModule: NetworkModule() {
    override fun baseUrl() = MockServer.server.url("/")
}
