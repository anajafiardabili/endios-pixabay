package de.endios.pixabay.mock

import de.endios.pixabay.ui.common.getJsonContent
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

fun successDispatcher(): Dispatcher {
    return object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when {
                request.path?.isNotEmpty() == true -> {
                    MockResponse().setResponseCode(200).setBody(
                        this.javaClass.classLoader!!.getJsonContent("photos.json")
                    )
                }
                else -> {
                    MockResponse().setResponseCode(400)
                }
            }
        }
    }
}

fun errorDispatcher(): Dispatcher {
    return object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when(request.path) {
                else -> MockResponse().setResponseCode(404).setBody("{ \"error\": \"error\"}")
            }
        }
    }
}


