package de.endios.pixabay.utils

import android.os.SystemClock

// TODO: This is not ideal. should be replaced with idling resources
fun waitForResults() {
    SystemClock.sleep(1500)
}
