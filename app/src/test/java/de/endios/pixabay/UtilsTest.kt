package de.endios.pixabay

import de.endios.pixabay.utils.prepareQueryString
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [30]) // otherwise get "Package targetSdkVersion=31 > maxSdkVersion=30" error
class UtilsTest {
    @Test
    fun `test calling prepareQueryString() should return encoded string correctly`() {
        val query = prepareQueryString("yellow flower")
        assertEquals("yellow+flower", query)
    }
}
