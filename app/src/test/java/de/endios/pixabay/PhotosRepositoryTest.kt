package de.endios.pixabay

import androidx.paging.AsyncPagingDataDiffer
import androidx.recyclerview.widget.ListUpdateCallback
import de.endios.pixabay.data.photo.FakePhotoRepository
import de.endios.pixabay.ui.photos.PhotoComparator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

// TODO need more tests here to cover our repository and RemoteMediator.
//  we can use https://developer.android.com/topic/libraries/architecture/paging/test
class PhotosRepositoryTest {

    // We wont use this object. It is just to be able to create something similar
    // to out adapter in the code
    private val updateCallback = object : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) {}
        override fun onRemoved(position: Int, count: Int) {}
        override fun onMoved(fromPosition: Int, toPosition: Int) {}
        override fun onChanged(position: Int, count: Int, payload: Any?) {}
    }

    @Test
    fun `test calling fake photos repository should return 3 photos`() = runBlocking {
        val repository = FakePhotoRepository()
        val adapter = AsyncPagingDataDiffer(
            diffCallback = PhotoComparator,
            updateCallback = updateCallback,
            mainDispatcher = Dispatchers.Unconfined,
            workerDispatcher = Dispatchers.Unconfined,
        )

        repository.getPhotos("sample query").collectLatest {
            adapter.submitData(it)
        }

        assertEquals(3, adapter.itemCount)
    }
}
