# Github Repositories

Amir's submission for Android assessment at endios.de

These are some screenshots of the app:
![Screenshot one](./img/screenshot_1.png)
![Screenshot two](./img/screenshot_2.png)
![Screenshot three](./img/screenshot_3.png)

And this is a animated gif showing the app in action:
![Demo](./img/demo.gif)

## Installing
* Download and install Android Studio. You can find the latest version at https://developer.android.com/studio
* Clone this repository on your machine
* Open the folder on the Android Studio

## Running the app
* From the toolbar, select the "app" configuration
* Select a device or an emulator. I tested the app on Pixel 3a emulator running Android 12
* Click on the green play button to run the app
  ![Running the app](./img/1.png)

## Running Unit Tests
* On Android Studio, open the project tree on the left panel
* Right click on the pixabay folder
* Click on "Run 'Tests in de.endios...'"  
  ![Running unit tests](./img/2.png)

## Running UI Tests
* On Android Studio, open the project tree on the left panel
* Right click on app/src/androidTest/java folder
* Click on "Run 'All Tests"  
  ![Running UI tests](./img/3.png)

## Technologies
I always use Google's Android Architecture Blueprints v2 (https://github.com/android/architecture-samples).

I tried to add a bit more to showcase some of the newer Android features that I have been using in my recent projects. I have tried to use:
* Google’s recommended app architecture (https://developer.android.com/jetpack/guide)
* MVVM
* Repository pattern
* ModelView
* LiveData
* Kotlin coroutines
* Material design
* Navigation component
* Jetpack Paging v3
* Dependency injection with Hilt
* Retrofit
* Room persistence library
* Unit testing
* UI testing (Espresso)

## Caching data

### Caching Pixabay response
I usually use a repository pattern (https://developer.android.com/jetpack/guide) with online/offline supports. Database is the single source of truth and repository retrieving remote data and persisting it locally for offline mode

But this project was different. It required pagination. I used Paging v3 (https://developer.android.com/topic/libraries/architecture/paging/v3-network-db) to implement repository pattern to load pages of photos from Pixabay into a local cache stored in a Room database.
![Repository with paging v3](./img/4.png)

### Caching images
I used Glide (https://github.com/bumptech/glide) to load images. It is recommended by Google and it has memory and disk caching internally.

## Other notes
I could not favorites field inside the Pixabay responses, so I did not implemented it.
I also used implemented a workaround recommended by Google to use Hilt for testing.
Description can be found here (https://developer.android.com/training/dependency-injection/hilt-testing#launchfragment) and here(https://homanhuang.medium.com/to-make-fragment-test-under-hilt-installed-65ff2d5e5eb6#7041)

## What will I differently if you had more time?
* More Unit/UI tests that covers repositories, database, viewmodels, and all the screens
* Better caching policy for the repositories
* Shared element transition animation between screens
* Using NetworkBoundResource
* Would add a lint checker like Ktlint
* Extract dimensions, text styles into resources

## Author
You can reach me at a.najafiardabili@gmail.com
